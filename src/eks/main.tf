    module "eks_fargate" {
      source  = "terraform-module/eks-fargate-profile/aws"
      version = "2.2.0"

      cluster_name         = "main-eks"
      subnet_ids           = concat(var.public_subnets, var.private_subnets)
      namespaces           = ["default"]
      labels = {
        "app.kubernetes.io/name" = "default-service"
       }
       tags = {
        "ENV" = "dev"
       }
    }