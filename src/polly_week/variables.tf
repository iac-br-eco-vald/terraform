variable github_token {
  type      = string
  sensitive = true
}

variable valdeco_domain_name {
  type        = string
}
