resource "aws_amplify_app" "polly_week" {
  name       = "polly_week"
  repository = "https://github.com/BCC-Allan/SitePoliWeek"
  access_token= var.github_token

  # The default build_spec added by the Amplify Console for React.
  build_spec = <<-EOT
    version: 0.1
    frontend:
      phases:
        preBuild:
          commands:
            - yarn install
        build:
          commands:
            - yarn run build
      artifacts:
        baseDirectory: dist
        files:
          - '**/*'
      cache:
        paths:
          - node_modules/**/*
  EOT

  # The default rewrites and redirects added by the Amplify Console.
  custom_rule {
    source = "/<*>"
    status = "404"
    target = "/index.html"
  }

}

resource "aws_amplify_branch" "main" {
  app_id      = aws_amplify_app.polly_week.id
  branch_name = "main"

  stage     = "PRODUCTION"

}

resource "aws_amplify_domain_association" "ds_subdomain" {
  app_id      = aws_amplify_app.polly_week.id
  domain_name = var.valdeco_domain_name

  sub_domain {
    branch_name = aws_amplify_branch.main.branch_name
    prefix      = "jsnow"
  }
}

resource "aws_amplify_webhook" "main" {
  app_id      = aws_amplify_app.polly_week.id
  branch_name = aws_amplify_branch.main.branch_name
  description = "triggermain"
}