variable gitlab_token_ds {
  type      = string
  sensitive = true
}

variable valdeco_domain_name {
  type        = string
}
