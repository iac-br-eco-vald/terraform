include "root" {
  path = find_in_parent_folders()
}

dependency "domain" {
  config_path = "../route53_valdecobr"
}

inputs = {
  valdeco_domain_name = dependency.domain.outputs.valdeco_zone.name
}