resource "aws_amplify_app" "deathstranding_helper" {
  name       = "deathstranding_helper"
  repository = "https://gitlab.com/valdemar-ceccon/deathstranding-helper"
  access_token= var.gitlab_token_ds

  # The default build_spec added by the Amplify Console for React.
  build_spec = <<-EOT
    version: 0.1
    frontend:
      phases:
        preBuild:
          commands:
            - yarn install
        build:
          commands:
            - yarn run build
      artifacts:
        baseDirectory: public
        files:
          - '**/*'
      cache:
        paths:
          - node_modules/**/*
  EOT

  # The default rewrites and redirects added by the Amplify Console.
  custom_rule {
    source = "/<*>"
    status = "404"
    target = "/index.html"
  }

}

resource "aws_amplify_branch" "main" {
  app_id      = aws_amplify_app.deathstranding_helper.id
  branch_name = "main"

  stage     = "PRODUCTION"

}

resource "aws_amplify_domain_association" "ds_subdomain" {
  app_id      = aws_amplify_app.deathstranding_helper.id
  domain_name = var.valdeco_domain_name

  sub_domain {
    branch_name = aws_amplify_branch.main.branch_name
    prefix      = "ds"
  }
}

resource "aws_amplify_webhook" "main" {
  app_id      = aws_amplify_app.deathstranding_helper.id
  branch_name = aws_amplify_branch.main.branch_name
  description = "triggermain"
}