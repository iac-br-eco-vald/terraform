include "root" {
  path = find_in_parent_folders()
}

inputs = {
  valdeco_zone_id = dependency.domain.outputs.valdeco_zone.zone_id
  valdeco_domain = dependency.domain.outputs.valdeco_zone.name
  vpc_id = dependency.vpc.outputs.vpc_id
  public_subnets = dependency.vpc.outputs.public_subnets
}

dependency "domain" {
  config_path = "../route53_valdecobr"
}

dependency "vpc" {
  config_path = "../vpc"
}
