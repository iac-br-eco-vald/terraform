resource "aws_instance" "lens" {
  ami           = var.instance_image
  instance_type = "t3.micro"
  key_name = aws_key_pair.valdemar_arch.key_name

  network_interface {
    network_interface_id = aws_network_interface.net_interface.id
    device_index         = 0
  }

  tags = {
    Name = "lens"
  }
}

resource "aws_network_interface" "net_interface" {
  subnet_id   = var.public_subnets[0]
  security_groups = [ aws_security_group.allow_tls.id ]
  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_eip" "public_eip" {
  vpc = true
  instance                  = aws_instance.lens.id
}

resource "aws_key_pair" "valdemar_arch" {
  key_name   = "valdemar_arch"
  public_key = var.public_ssh_key
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

    ingress {
      description      = "TLS from VPC"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

    ingress {
      description      = "TLS from VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }  

  egress {
      description      = "TLS from VPC"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_route53_record" "lens" {
  zone_id = var.valdeco_zone_id
  name    = "lens.${var.valdeco_domain}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.public_eip.public_ip]
}

resource "aws_route53_record" "translate" {
  zone_id = var.valdeco_zone_id
  name    = "translate.${var.valdeco_domain}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.public_eip.public_ip]
}