variable "public_ssh_key" {  
}

variable allowed_ips {
    type = list(string)
}

variable "instance_image" {
  default = "ami-083654bd07b5da81d"
}

variable "valdeco_zone_id" {
}

variable "valdeco_domain" {
}

variable "vpc_id" {  
}

variable "public_subnets" {
    type = list(string)
}

